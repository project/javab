<?php

class JavaCommand {
  
  private $options;
  
  public function __construct($options = NULL) {
    $this->options = is_array($options) ? $options : array();
    // TODO: add options.
    
    if (isset($GLOBALS['java_home'])) {
      $java_home = $GLOBALS['java_home'];
    } else if (ini_get('java.home') != NULL) {
      $java_home = ini_get('java.home');
    } else if (isset($_ENV['JAVA_HOME'])) {
      $java_home = $_ENV['JAVA_HOME'];
    } else {
      trigger_error("Cannot find JAVA_HOME", E_USER_ERROR);
      $java_home = ''; // undefined.
    }
    $this->setOption('java_home', $java_home);
    
  }
  
  public function setOption($key, $value) {
    $this->options[$key] = $value;
  }
  
  public function getCommand() {
    $cmd = $this->options['java_home'] .'/bin/java';
    
    if (isset($this->options['Xms'])) {
      $cmd .= " -Xms{$this->options['Xms']}";
    }
    if (isset($this->options['Xmx'])) {
      $cmd .= " -Xmx{$this->options['Xmx']}";
    }
    
    foreach ($this->options as $key => $value) {
      if (strpos($key, 'D') === 0) {
        $cmd .= " -{$key}=". escapeshellarg($value);
      }
    }
    
    if (isset($this->options['jar'])) {
      $cmd .= " -jar {$this->options['jar']}";
    } else {
      trigger_error("Please specifiy JAR file", E_USER_ERROR);
    }
    
    return $cmd;
  }
  
  public function run() {
    $cmd = $this->getCommand();
    system($cmd, $ret);
    return $ret;
  }
  
}

?>