java-bridge module
-- that provides java interface for Drupal

README

included modules:
commons-cli
commons-dbcp
phpserializer

functions:
readSettings
get/set Variables
setup etc.

-----------------------
Note on 2009-6-28

./src contains the Java files.
org.drupal.project.javab.*: The Java classes for Drupal. Each application will derive a sub-class from it.
magicstudio.*: test java files.

javademo.xml & javademo.properties: Ant build file to build javademo.jar.