package magicstudio;

import java.util.List;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {
		memoryTest();
    }

	public static void memoryTest() throws Exception {
		List list = new ArrayList();
        for (int i=0; i<10; i++) {
            System.out.println("Trying to allocate "+(i+1)+"0M.");
            list.add(new byte[10*(int) Math.pow(2,20)]); 
        }
        System.out.println("Start running. Wait 3 mins.");
        for (int i=0; i<5; i++) {
            System.out.println("Tick: " + (i+1));
            Thread.sleep(1000);
        }
        System.out.println("Finish running.");
	}
}
